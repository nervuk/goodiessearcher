const searchEngines = {
  common: {
    datatypes: [
      {
        type: 'json',
        extension: 'json'
      }
    ],
  },
  urls: [
    {
      name: 'prom',
      protocol: 'https://',
      domain: 'prom.ua',
      url: '/search?search_term=',
      page: '',
      spaceAs: ' ',
      method: 'GET',
    },
    {
      name: 'google',
      protocol: 'https://',
      domain: 'www.google.com',
      url: '/search?q=',
      page: '',
      spaceAs: '+',
      method: 'GET',
    },
    {
      name: 'duckduckgo',
      protocol: 'https://',
      domain: 'api.duckduckgo.com',
      url: '?q=<search_query>&format=json&pretty=1',
      page: '',
      spaceAs: '+',
      method: 'GET',
      datatype: 'json',
    },
    {
      name: 'duckduckgo1',
      protocol: 'https://',
      domain: 'duckduckgo.com',
      url: '/?hps=1&q=',
      page: '',
      spaceAs: '+',
      method: 'GET',
    },
    {
      name: 'startpage',
      protocol: 'https://',
      domain: 'www.startpage.com',
      url: '/do/search?q=',
      page: '',
      spaceAs: '+',
      method: 'GET',
    },
    {
      name: 'startpage1',
      protocol: 'https://',
      domain: 'www.startpage.com',
      url: '/?query=',
      page: '',
      spaceAs: '+',
      method: 'POST',
    },
    {
      name: 'meta',
      protocol: 'https://',
      domain: 'search.meta.ua',
      url: '?q=',
      page: '&gsc.page=',
      spaceAs: '',
      method: 'GET',
    },
    {
      name: '',
      protocol: '',
      domain: '',
      url: '',
      page: '',
      spaceAs: '',
      method: 'GET',
    },
    {
      name: '',
      protocol: '',
      domain: '',
      url: '',
      page: '',
      spaceAs: '',
      method: 'GET',
    },
  ]
};

export default searchEngines;