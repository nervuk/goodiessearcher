'use strict';

import fs from 'node:fs';
import path from 'path';

const createDirectory = (directoryPath) => {
  if (!fs.existsSync(directoryPath)) {
    fs.mkdirSync(directoryPath, { recursive: true });
  }
};

const saveHTML = (data, dest) => {
  if (data) {
    createDirectory(dest.dir);
    fs.writeFile(path.join(dest.dir, dest.file), data, (err) => {
        if (err) throw err;
      console.log(`New file size: ${data.length}`);
    });
  }
}

export { saveHTML };