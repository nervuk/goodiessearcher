'use strict'

import { request } from 'undici';

const httpGET = async (protocol, domain, url) => {
  const urlPath = `${protocol}${domain}${url}`;
  
  const response = await request(urlPath)
  const {
    statusCode,
    headers,
    trailers,
    body
  } = response;
  let result = null;
  if (statusCode === 200) {
    return result = await body.text();
  } else if (statusCode === 302 || statusCode === 301) {
    return await httpGET(protocol, domain, headers.location);
  } else {
    console.log(`statusCode: ${statusCode}`);
  }
}
export { httpGET };