//import { httpGET } from './src/get.js'
import { httpGET } from './src/fetch.js'
import { saveHTML } from './src/saveHTML.js'
import se from './src/urls.js'
import cfg from './config/app,js'

const searchStr = 'grub'

const site = se.urls.find(url => url.name === 'duckduckgo');
if (!site) exit;

let searchExpr = (site.spaceAs !== ' ') ? searchStr.replace(' ', site.spaceAs) : searchStr;
searchExpr = site.url.replace('<search_query>', searchExpr);
console.log(searchExpr);
const getData = async (protocol, domain, url, dest) => {
  const body = await httpGET(protocol, domain, url);
  console.log(body);
  await saveHTML(body, dest);
}

const composeDestination = (datatypes, dest, dtype) => {
  const datatype = datatypes.find(el => el.type === dtype);

  return {
    dir: dest.dir,
    file: `${dest.file}${(datatype) ? `.${datatype.extension}` : ''}`,
  };
}

const destination = composeDestination(se.common.datatypes, cfg.destination, site.datatype);

getData(site.protocol, site.domain, searchExpr, destination);
